Source: xalan
Priority: extra
Section: text
Maintainer: William Blough <devel@blough.us>
Build-Depends: debhelper (>= 9), dpkg-dev (>= 1.16), autotools-dev, libxerces-c-dev, autoconf, automake, dh-autoreconf, dh-exec (>= 0.3)
Build-Depends-Indep: doxygen
Standards-Version: 3.9.6
Homepage: https://xalan.apache.org/xalan-c/
Vcs-Git: https://bitbucket.org/bblough/xalan.git
Vcs-Browser: https://bitbucket.org/bblough/xalan/src

Package: libxalan-c111
Architecture: any
Multi-Arch: same
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: XSLT processor library for C++
 This package provides the runtime library for Xalan, the XSLT processor
 from the Apache Software Foundation.  This library provides support for
 applications to transform XML documents to other formats using XSLT 
 templates.

Package: libxalan-c-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends}, libxalan-c111 (= ${binary:Version}), libxerces-c-dev, libc6-dev
Replaces: libxalan110-dev
Conflicts: libxalan110-dev
Description: XSLT processor library for C++ [development]
 This package provides development headers needed to develop applications
 that use the Xalan XSLT processing library from the Apache Software 
 Foundation.

Package: libxalan-c-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: libxalan-c-dev
Description: XSLT processor library for C++ [development docs]
 This package contains the API documentation and programming reference 
 .
 Users of applications that rely on Xalan will not need to install this,
 however developers of applications that rely on Xalan may find this
 package helpful.


Package: xalan
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: XSLT processor utility
 This package provides a command line utility to transform XML documents
 to other formats using XSLT templates.  It uses the Xalan XSLT processing
 library from the Apache Software Foundation.
